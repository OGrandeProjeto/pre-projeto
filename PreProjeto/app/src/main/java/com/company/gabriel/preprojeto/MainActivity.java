package com.company.gabriel.preprojeto;

// Importação das bibliotecas
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

//Classe principal (MainActivity)
public class MainActivity extends AppCompatActivity {
    // Declaração das variaveis utilizadas no XML e para o armazenamento no banco de dados.

    private EditText editEmail, editSenha;
    private Button btnLogar, btnCadastro;
    private TextView txtResetSenha;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Funções para inicializar os componentes e o momento do click.
        inicializarComponentes();
        eventoClicks();
    }

    private void eventoClicks() {
        //Evento para quando haver o click no botao Logar, onde o campo email e senha sao direcionados para a função (login).
        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editEmail.getText().toString().trim();
                String senha = editSenha.getText().toString().trim();
                login(email, senha);
            }
        });
        //Evento para quando haver o click no botao Cadastro, ser direcionado para o layout do cadastro.
        btnCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Cadastro.class);
                startActivity(i);
            }
        });
        //Evento para quando haver o click na mensagem de Esqueceu a senha, o app é direcionado a pagina de ResetSenha.
        txtResetSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ResetSenha.class);
                startActivity(i);
            }
        });
    }
    //Função que vai fazer o login utilizando o cadastro ja feito no banco de dados.(TEM Q ANALIZAR QUANDO NAO HA NADA ESCRITO NOS CAMPOS LOGIN E SENHA).
    private void login(String email, String senha) {
        auth.signInWithEmailAndPassword(email, senha)
                .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //Função que ira checar se o usuario fez a comfirmação pelo email.
                            checarEmail();
                        }else{
                            alert("E-mail ou Senha Incorretos.");
                        }
                    }
                });
    }
    //Método que ira verificar a confirmação do email para o login do usuario.
    private void checarEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        assert user != null;
        if(user.isEmailVerified()){
            btMenu();
        }else{
            alert("E-mail ainda não autenticado. Por favor entre na caixa de e-mail.");
        }
    }

    //Função de alerta para algum erro
    private void alert(String s) {
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    private void inicializarComponentes() {
        editEmail =(EditText) findViewById(R.id.campoCadastroEmail);
        editSenha = (EditText) findViewById(R.id.campoLoginSenha);
        btnCadastro = (Button) findViewById(R.id.btnLoginCadastro);
        btnLogar = (Button) findViewById(R.id.btnLoginLogar);
        txtResetSenha = (TextView) findViewById(R.id.txtResetSenha);
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth = Conexao.getFirebaseAuth();
    }

    //Método que vai fazer a chamada do Menu.
    private void btMenu(){
        startActivity(new Intent(MainActivity.this, MenuPrincipal.class));
        finish();
    }
}
