package com.company.gabriel.preprojeto;

// Importação das bibliotecas
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

//Classe principal (Cadastro)
public class Cadastro extends AppCompatActivity {
    // Declaração das variaveis utilizadas no XML e para o armazenamento no banco de dados.

    private EditText editEmail, editSenha, editNome;
    private Button btnCadastrar, btnVoltar;
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        //Funções para inicializar os componentes e o momento do click.
        inicializaComponentes();
        eventoClicks();
    }

    private void eventoClicks() {
        //Evento para quando haver o click no botao cadastrar, ele recebe as informações do campo email e do campo senha emanda para a função criarUser.
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editEmail.getText().toString().trim();
                String senha = editSenha.getText().toString().trim();
                criarUser(email, senha);
            }
        });
        //Evento para quando haver o click no botao voltar, ele retorna a pagina de login.
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    //Função para fazer o cadastro no Firebase.
    private void criarUser(String email, String senha) {
        auth.createUserWithEmailAndPassword(email, senha)
                .addOnCompleteListener(Cadastro.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            alert("Usuario Cadastrado com Sucesso.");
                            //Função para fazer o envio da Autentificação do usuario pelo email.
                            verificarEmail();
                        }else{
                            alert("Erro de Cadastro");
                        }
                    }
                });
    }
    //Método que irá enviar o email de confirmação para o usuario apos o cadastro.
    private void verificarEmail() {
        final FirebaseUser user = auth.getCurrentUser();

        assert user != null;
        user.sendEmailVerification().addOnCompleteListener(Cadastro.this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    FirebaseAuth.getInstance().signOut();
                    alert("Verificação do e-mail enviada.");
                    Intent i = new Intent(Cadastro.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    alert("Erro na verificação do e-mail.");
                }
            }
        });
    }

    private void inicializaComponentes() {
        editEmail = (EditText) findViewById(R.id.campoCadastroEmail);
        editSenha = (EditText) findViewById(R.id.campoCadastroSenha);
        editNome = (EditText) findViewById(R.id.campoCadastroNome);
        btnCadastrar = (Button) findViewById(R.id.btnCadastroCadastrar);
        btnVoltar = (Button) findViewById(R.id.btnCadastroVoltar);
    }

    @Override
    public void onStart() {
        super.onStart();
        auth = Conexao.getFirebaseAuth();
    }

    // Método responsável por exibir mensagem de email e senhas diferentes (TEM Q COLOCAR ISSO DEPOIS, FIZ SO O BASICO)
    private void alert(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }
}
