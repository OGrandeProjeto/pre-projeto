package com.company.gabriel.preprojeto;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetSenha extends AppCompatActivity {
    // Declaração das variaveis utilizadas no XML e para o armazenamento no banco de dados.
    private EditText editEmail;
    private Button btnResetSenha, btnResetVoltar;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_senha);
        //Funções para inicializar os componentes e o momento do click.
        inicializarComponentes();
        eventoClicks();
    }

    private void eventoClicks() {
        //Evento para quando haver o click no botao resetar senha, recebe o email e é direcionado para a função resetSenha.
        btnResetSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editEmail.getText().toString().trim();
                resetSenha(email);
            }
        });
        //Evento para quando haver o click no botao de voltar, ele retornar a pagina de login.
        btnResetVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    //Metodo que ira receber o email e ira sincronizar com o banco de dados e enviar o email de reset.
    private void resetSenha(String email) {
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(ResetSenha.this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            alert("Um email foi encaminhado");
                            //Intent i = new Intent(ResetSenha.this, MainActivity.class);
                            //startActivity(i);
                            finish();
                        }else{
                            alert("E-mail não Registrado");
                        }
                    }
                });
    }
    //Metodo de alerta de mensagem
    private void alert(String s) {
        Toast.makeText(ResetSenha.this, s, Toast.LENGTH_SHORT).show();
    }


    private void inicializarComponentes() {
        editEmail = (EditText) findViewById(R.id.editResetEmail);
        btnResetSenha = (Button) findViewById(R.id.btnResetSenha);
        btnResetVoltar = (Button) findViewById(R.id.btnResetVoltar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth = Conexao.getFirebaseAuth();
    }

}
