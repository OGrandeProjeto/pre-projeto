package com.company.gabriel.preprojeto;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtem o SupportMapFragment para depois ser utilizado.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    //Apos o mapa abrir, sera feita as modificacoes, adicao e implementacao das info do mapa.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //Permissao de Acesso para utilizar o Google Play Services e conseguir a localizacao do dispositivo.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //Necessario para utilziar setMyLocationEnabled, que identifica a localizacao do dispositivo.
        mMap.setMyLocationEnabled(true);
        //Colocando um marcador em uma posicao definida, no caso em questao a Ufes, utiliza Latitude e Longitude
        LatLng Ceunes = new LatLng(-18.6758348, -39.8645768);
        mMap.addMarker(new MarkerOptions().position(Ceunes).title("Ufes"));
        //Marcador do RU
        LatLng RU = new LatLng(-18.676663,-39.8625946);
        mMap.addMarker(new MarkerOptions().position(RU).title("Restaurante Universitario"));
        //Marcador da Supgrad
        LatLng Supgrad = new LatLng(-18.6767313,-39.8602247);
        mMap.addMarker(new MarkerOptions().position(Supgrad).title("Supgrad"));
        //Marcador do DECH
        LatLng DECH = new LatLng(-18.6769311,-39.86367);
        mMap.addMarker(new MarkerOptions().position(DECH).title("Dep. De Educacao e Ciencias Humanas"));
        //Marcador do Laboratorio de Eletronica
        LatLng LabEletro = new LatLng(-18.6758798,-39.8628239);
        mMap.addMarker(new MarkerOptions().position(LabEletro).title("Laboratorio de Eletronica"));
        //Marcador da Adapti
        LatLng Adapti = new LatLng(-18.6758798,-39.8628239);
        mMap.addMarker(new MarkerOptions().position(Adapti).title("Adapti"));
    }
}
